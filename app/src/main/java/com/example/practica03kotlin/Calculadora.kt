package com.example.practica03kotlin

class Calculadora {
    var num1: Float = 0f
    var num2: Float = 0f
    constructor(num1:Float,num2:Float){

        this.num1 = num1
        this.num2 = num2

    }
    fun suma():Float {
        return num1+num2
    }
    fun resta():Float{
        return num1-num2
    }
    fun multiplicacion():Float{
        return num1*num2
    }
    fun division():Float{
        var total = 0f;
        if(num2!=0f){
            total = num1/num2
        }
        return total
    }


}