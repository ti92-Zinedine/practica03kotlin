package com.example.practica03kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

private lateinit var btnSumar : Button
private lateinit var btnRestar : Button
private lateinit var btnMulti : Button
private lateinit var btnDiv : Button
private lateinit var btnLimpiar : Button
private lateinit var btnRegresar : Button
private lateinit var lblUsuario : TextView
private lateinit var lblResultado : TextView
private lateinit var txtUno : EditText
private lateinit var txtDos : EditText

// Declarar el objeto Calculadora
private var calculadora = Calculadora(0f, 0f)

class CalculadoraActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)
        iniciarComponentes()
        // Obtener los datos del MainActivity
        var datos = intent.extras
        var usuario = datos!!.getString("usuario")
        lblUsuario.text = usuario.toString()


        btnSumar.setOnClickListener { btnSumar() }
        btnRestar.setOnClickListener { btnRestar() }
        btnMulti.setOnClickListener { btnMultiplicar() }
        btnDiv.setOnClickListener { btnDividir() }

        btnLimpiar.setOnClickListener { btnLimpiar() }
        btnRegresar.setOnClickListener { btnRegresar() }
    }

    private fun iniciarComponentes(){

        btnSumar = findViewById(R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)
        btnMulti = findViewById(R.id.btnMultiplicar)
        btnDiv = findViewById(R.id.btnDividir)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)

        lblUsuario = findViewById(R.id.lblUsuario)
        lblResultado = findViewById(R.id.lblResultado)

        txtUno = findViewById(R.id.txtNum1)
        txtDos = findViewById(R.id.txtNum2)
    }
    fun btnSumar(){
        if(txtUno.text.toString().isEmpty() || txtDos.text.toString().isEmpty()){
            Toast.makeText(this, "Por favor, ingrese un número", Toast.LENGTH_SHORT).show()
        }else{
            calculadora.num1 = txtUno.text.toString().toFloat()
            calculadora.num2 = txtDos.text.toString().toFloat()
            var total = calculadora.suma()
            lblResultado.text = total.toString()
        }
    }
    fun btnRestar(){
        if(txtUno.text.toString().isEmpty() || txtDos.text.toString().isEmpty()){
            Toast.makeText(this, "Por favor, ingrese un número", Toast.LENGTH_SHORT).show()
        }else{
            calculadora.num1 = txtUno.text.toString().toFloat()
            calculadora.num2 = txtDos.text.toString().toFloat()
            var total = calculadora.resta()
            lblResultado.text = total.toString()
        }
    }
    fun btnMultiplicar(){
        if(txtUno.text.toString().isEmpty() || txtDos.text.toString().isEmpty()){
            Toast.makeText(this, "Por favor, ingrese un número", Toast.LENGTH_SHORT).show()
        }else{
            calculadora.num1 = txtUno.text.toString().toFloat()
            calculadora.num2 = txtDos.text.toString().toFloat()
            var total = calculadora.multiplicacion()
            lblResultado.text = total.toString()
        }
    }
    fun btnDividir(){
        if(txtUno.text.toString().isEmpty() || txtDos.text.toString().isEmpty()){
            Toast.makeText(this, "Por favor, ingrese un número", Toast.LENGTH_SHORT).show()
        }else{
            calculadora.num1 = txtUno.text.toString().toFloat()
            calculadora.num2 = txtDos.text.toString().toFloat()
            var total = calculadora.division()
            lblResultado.text = total.toString()
        }
    }

    fun btnLimpiar(){
        if(txtUno.text.toString().isEmpty() && txtDos.text.toString().isEmpty()){
            Toast.makeText(this, "Los campos ya están vacíos", Toast.LENGTH_SHORT).show()
        }else{
            lblResultado.setText("")
            txtUno.setText("")
            txtDos.setText("")
        }

    }

    fun btnRegresar(){
        var confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage("Regresar al MainActivity")
        confirmar.setPositiveButton("Confirmar"){dialogInterface,which->finish()}
        confirmar.setNegativeButton("Cancelar"){dialogInterface,which->}
        confirmar.show()
    }

}